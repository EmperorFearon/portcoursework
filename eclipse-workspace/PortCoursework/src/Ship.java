import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Ship {	
	private static String dataDir = "src" + File.separator + "ShipData.txt";
	private static File shipData = new File(dataDir).getAbsoluteFile();

	private String name;
	private int size;

	public static ArrayList<Ship> list = new ArrayList<>();

	public Ship(String shipName, int shipSize) {
		this.name = shipName;
		this.size = shipSize;		
	}

	public Ship(String shipName) {
		this.name = shipName;
	}

	public String getShipName() {
		return name;
	}

	public int getShipSize() {
		return size;
	}

	/**
	 * Loads data from the ShipData.txt provided and parses said data into and object arraylist for use with program.
	 * @throws FileNotFoundException
	 * @throws ParseException
	 */
	public static void loadShips() throws FileNotFoundException, ParseException {
		Scanner s = new Scanner(new FileInputStream(shipData));
		String name;
		int size;

		for(@SuppressWarnings("unused")
		int n = 0; s.hasNext(); n++) {
			String nextLine = s.nextLine();
			String [] strs = nextLine.split(",");

			name = strs[0];
			size = Integer.parseInt(strs[1]);
			Ship a = new Ship(name, size);
			list.add(a);
		}
		s.close();
	}

	/**
	 * Displays the ships which have been parsed into the ship arraylist
	 */
	public static void listShips() {
		for (int n = 0; n < list.size(); n++) {
			System.out.println(list.get(n).getShipName());
		}
	}

	/*
	 * Takes new ship's information from user and adds it to the arraylist
	 */
	public static void dockShip() {
		String name = null, size = null;
		int sizeParsed = 0;
		boolean flag = false;
		while (!flag) {
			Scanner s = new Scanner(System.in);
			System.out.print("Please enter ship name: ");
			name = s.nextLine();
			System.out.println("Please enter ship size: ");
			System.out.println("1. Small");
			System.out.println("2. Medium");
			System.out.println("3. Large");
			size = s.nextLine();

			switch(size){
			case "1":
				sizeParsed = 1;
				break;
			case "2":
				sizeParsed = 2;
				break;
			case "3":
				sizeParsed = 3;
				break;
			}

			System.out.println("Is the infomation correct? (Y/N):");
			System.out.println("Ship name: " + name);
			System.out.println("Ship size: " + sizeParsed);
			String res = s.next().toUpperCase();
			switch(res) {
			case "Y":
				flag = true;
				break;
			}
		}
		Ship e = new Ship(name, sizeParsed);
		list.add(e);
		Dock.dockShip(e);
	}

}
