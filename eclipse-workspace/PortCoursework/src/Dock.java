import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Dock {
	private static String dataDir = "src" + File.separator + "RowData.txt";
	private static File rowData = new File(dataDir).getAbsoluteFile();

	private int size;	
	private boolean isOccupied = false;
	private String shipName = null;

	public static ArrayList<Dock> row = new ArrayList<Dock>();
	public static String[] waitingList = new String[4];						//Waiting list is set size, therefore does not require dynamic capabilities
	
	public Dock(int size) {
		this.size = size;
		this.isOccupied = false;
		this.shipName = null;
	}
	
	public Dock() {
		isOccupied = false;
		this.shipName = null;
	}

	public Dock(int size, String sN) {
		this.size = size;
		this.isOccupied = true;
		this.shipName = sN;
	}
	
	public String getShipName() {
		return shipName;
	}

	public int getRowSize(){
		return size;
	}

	public boolean getIsOccupied() {
		return isOccupied;
	}

	/**
	 * Loads data from the RowData.txt provided and parses said data into and object arraylist for use with program.
	 * @throws FileNotFoundException
	 */
	public static void loadRows() throws FileNotFoundException {
		Scanner s = new Scanner(new FileInputStream(rowData));
		int size;

		for(int n = 0; s.hasNext(); n++) {
			size = Integer.parseInt(s.nextLine());

			Dock a = new Dock(size);
			row.add(a);
		}
		s.close();
	}

	/**
	 * Lists rows which have been parsed as well as their current occupation status. Can be used to check current status of the dock
	 */
	public static void listRows() {
		String leftAlignFormat = "| %-10s | %-10s | %-10s | %n";

		System.out.format("+------------+------------+------------+%n");
		System.out.format("| Row ID     | Occupied   |Ship Name   +%n");
		System.out.format("+------------+------------+------------+%n");
		for (int n = 0; n < row.size(); n++) {
			System.out.format(leftAlignFormat, sizeToString(row.get(n).getRowSize()), Boolean.toString(row.get(n).getIsOccupied()), row.get(n).getShipName());
		}
		System.out.format("+------------+------------+------------+%n");
	}

	/**
	 * Assigns an available location for a ship to be docked 
	 */
	public static void dockShip(Ship e) {
		for(int n = 0; n < row.size(); n++) {
			if (row.get(n).size >= e.getShipSize() && !row.get(n).isOccupied) {
				Dock s = new Dock(row.get(n).size, e.getShipName());
				row.set(n, s);
				break;
			}
		}
	}

	/**
	 * Adds a ship to the waiting list
	 * @param shipName
	 */
	private static void addToWaitingList(String shipName) {
		for (int n = 0; n <= waitingList.length; n++) {
			waitingList[n] = shipName;
		}
	}

	/**
	 * Removes ship from space and makes said space available for another ship to use
	 * 
	 */
	public static void removeShip(Ship e) {		
		for(int n = 0; n < row.size(); n++) {
			String lookingFor = row.get(n).getShipName();
			String searchingWith = e.getShipName();
			
			if(lookingFor == searchingWith) {
				System.out.println("Ship found! Removing...");
				Dock x = new Dock();
				row.set(n, x);
			}
		}
	}
	
	/**
	 * Requests user input to find which ship they wish to undock
	 * @return
	 */
	public static Ship undockShip() {
		Scanner s = new Scanner(System.in);
		String shipName;
		
		System.out.print("Please enter ship name: ");
		shipName = s.next();
		
		Ship e = new Ship(shipName);
		return e;
	}

	/**
	 * Parses the size from an integer value to a string, providing better readability when displaying sizes to user
	 * @param n
	 * @return
	 */
	public static String sizeToString(int n) {
		String res = null;

		if(n == 1) {
			res = "Small";
		}else if(n == 2) {
			res = "Medium";
		}else if(n == 3) {
			res = "Large";
		}
		return res;
	}
}