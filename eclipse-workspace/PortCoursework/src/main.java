import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Scanner;

public class main {

	public static void main(String[] args){
		try {														//Loading data from text files
			Ship.loadShips();
			Dock.loadRows();
		} catch (FileNotFoundException | ParseException e) {
			System.out.println("-- ERROR IMPORTING DATA --");
			System.out.println("-- TERMINATING... --");
			e.printStackTrace();
		}
		
		displayMenu();
	}
	
	/**
	 * Prints out available functions for this application.
	 * Features validation techniques, case/switch, and user input
	 */
	public static void displayMenu() {
		boolean flag = false;
		
		System.out.println("-- Port Simulator --");
		
		do {
			Scanner s = new Scanner(System.in);
			System.out.println("1. Dock ship");
			System.out.println("2. Undock ship");
			System.out.println("3. View ship status");
			System.out.println("Q. Quit");
			String n = s.next();
			
			switch(n.toUpperCase()) {
			case "1":
				Ship.dockShip();
				break;
			case "2":
				Dock.removeShip(Dock.undockShip());
				break;
			case "3":
				Dock.listRows();
				break;
			case "Q":
				flag = true;
				s.close();
				break;
			}
		}while(!flag);
	}
	
}
