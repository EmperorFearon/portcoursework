# Port Coursework #

This program has been developed as my submittion to 7129COMP - Software Development With Java.  
It has been made to simulate the system of a port, which is used by boats to stop by and set off.  
It has been made using .txt files for the data which the program uses. This has been done with the intention of being edited to suit any port that would wish to use it.

### Program Features ###

* Boats occupying and setting off from spaces
* Waiting list for boats unable to berth
* Space allocation algorithm

### How do I get set up? ###

Edit rowData.txt to match your use case, using the same format used by example data.  
shipData.txt simply hosts examples of ships used for testing and does not require any changes.

### Author ###

Liam Fearon  
775046  
cmplfear@ljmu.ac.uk